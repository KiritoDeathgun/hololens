using System;
using System.Collections;
using System.Collections.Generic;
using Microsoft.MixedReality.Toolkit.Input;
using UnityEngine;
using Random = UnityEngine.Random;

[RequireComponent(typeof(Rigidbody))]
public class FallingObject : MonoBehaviour, IMixedRealityTouchHandler
{
    public void OnTouchStarted(HandTrackingInputEventData eventData)
    {
        StartCoroutine(nameof(Dissolve));
        //throw new NotImplementedException();
    }

    public void OnTouchCompleted(HandTrackingInputEventData eventData)
    {
        //throw new NotImplementedException();
    }

    public void OnTouchUpdated(HandTrackingInputEventData eventData)
    {
        //throw new NotImplementedException();
    }

    [Range(0.001f,0.005f)]
    public float minFallingSpeed;
    [Range(0.005f,0.05f)]
    public float maxFallingSpeed;
    private float fallingSpeed;
    private Rigidbody rb;

    public Material material;
    [Range(0.0f, 1.0f)]
    public float cutoff;

    public float cutoffSpeed;

    private void Start()
    {
        rb = GetComponent<Rigidbody>();
        material = GetComponent<MeshRenderer>().material;
        
    }

    private void Update()
    {
        material.SetFloat("_Cutoff", cutoff);
    }

    private IEnumerator Dissolve()
    {


        
        Debug.LogError("Dissolve by Touch");
        this.gameObject.SetActive(false);
        
        yield break;
    }

    private IEnumerator DissolveAfterFiveSeconds()
    {
        yield return new WaitForSeconds(5.0f);
        
        while (cutoff < 1)
        {

            cutoff += 0.01f;
            yield return new WaitForSeconds(0.01f);
        }
        
        Debug.LogError("Dissolve after 5 seconds");
        //this.gameObject.SetActive(false);
    }
    
    private void FixedUpdate()
    {
        rb.AddForce(Physics.gravity * rb.mass * fallingSpeed);
    }

    private void OnEnable()
    {
        fallingSpeed = Random.Range(minFallingSpeed, maxFallingSpeed);
        StartCoroutine(nameof(DissolveAfterFiveSeconds));
        
    }
}
