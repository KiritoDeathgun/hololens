using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ObjectSpawner : MonoBehaviour
{

    private ObjectPooler OP;

    public float spawnRadius;
    public float minSpawnRadius = 1;
    
    public float spawnHeight;
    public int timeToRun;
    public int amountOfObjects;
    private int objectsSpawned = 0;
    

    private GameObject camera;
    
    // Start is called before the first frame update
    void Start()
    {
        OP = ObjectPooler.SharedInstance;
        camera = Camera.main.gameObject;
        

        if (timeToRun != 0)
        {
            amountOfObjects = timeToRun / 5;
        }

        StartCoroutine(nameof(SpawningCoroutine));
    }

    IEnumerator SpawningCoroutine()
    {
        yield return new WaitForSeconds(1f);
        while (amountOfObjects > objectsSpawned)
        {
            float randomx = 0;
            float randomz = 0;

            while (randomx < minSpawnRadius && randomx > -minSpawnRadius)
            {
                randomx = Random.Range(-spawnRadius, spawnRadius);
            }
            
            while (randomz < minSpawnRadius && randomz > -minSpawnRadius)
            {
                randomz = Random.Range(-spawnRadius, spawnRadius);
            }

            int randomitem = Random.Range(0, OP.itemsToPool.Count);
            spawnHeight = camera.transform.position.y;

            var test = OP.GetPooledObject(randomitem);
            test.SetActive(true);
            test.transform.position = new Vector3(randomx,spawnHeight,randomz);

            yield return new WaitForSeconds(1f);
            objectsSpawned++;
        }
    }
}
